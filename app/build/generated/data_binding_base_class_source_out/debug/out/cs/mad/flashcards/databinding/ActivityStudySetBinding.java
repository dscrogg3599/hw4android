// Generated by view binder compiler. Do not edit!
package cs.mad.flashcards.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
import cs.mad.flashcards.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityStudySetBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final ConstraintLayout buttonLayout;

  @NonNull
  public final CardView card1;

  @NonNull
  public final CardView card2;

  @NonNull
  public final CardView card3;

  @NonNull
  public final ConstraintLayout cardConstraintLayout;

  @NonNull
  public final TextView cardText;

  @NonNull
  public final TextView completedText;

  @NonNull
  public final Button correctButton;

  @NonNull
  public final TextView correctCounter;

  @NonNull
  public final MaterialButton exitButton;

  @NonNull
  public final Button missedButton;

  @NonNull
  public final TextView missedCounter;

  @NonNull
  public final TextView outOfText;

  @NonNull
  public final Button skipButton;

  private ActivityStudySetBinding(@NonNull ConstraintLayout rootView,
      @NonNull ConstraintLayout buttonLayout, @NonNull CardView card1, @NonNull CardView card2,
      @NonNull CardView card3, @NonNull ConstraintLayout cardConstraintLayout,
      @NonNull TextView cardText, @NonNull TextView completedText, @NonNull Button correctButton,
      @NonNull TextView correctCounter, @NonNull MaterialButton exitButton,
      @NonNull Button missedButton, @NonNull TextView missedCounter, @NonNull TextView outOfText,
      @NonNull Button skipButton) {
    this.rootView = rootView;
    this.buttonLayout = buttonLayout;
    this.card1 = card1;
    this.card2 = card2;
    this.card3 = card3;
    this.cardConstraintLayout = cardConstraintLayout;
    this.cardText = cardText;
    this.completedText = completedText;
    this.correctButton = correctButton;
    this.correctCounter = correctCounter;
    this.exitButton = exitButton;
    this.missedButton = missedButton;
    this.missedCounter = missedCounter;
    this.outOfText = outOfText;
    this.skipButton = skipButton;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityStudySetBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityStudySetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_study_set, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityStudySetBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.button_layout;
      ConstraintLayout buttonLayout = rootView.findViewById(id);
      if (buttonLayout == null) {
        break missingId;
      }

      id = R.id.card1;
      CardView card1 = rootView.findViewById(id);
      if (card1 == null) {
        break missingId;
      }

      id = R.id.card2;
      CardView card2 = rootView.findViewById(id);
      if (card2 == null) {
        break missingId;
      }

      id = R.id.card3;
      CardView card3 = rootView.findViewById(id);
      if (card3 == null) {
        break missingId;
      }

      id = R.id.card_constraint_layout;
      ConstraintLayout cardConstraintLayout = rootView.findViewById(id);
      if (cardConstraintLayout == null) {
        break missingId;
      }

      id = R.id.card_text;
      TextView cardText = rootView.findViewById(id);
      if (cardText == null) {
        break missingId;
      }

      id = R.id.completedText;
      TextView completedText = rootView.findViewById(id);
      if (completedText == null) {
        break missingId;
      }

      id = R.id.correct_button;
      Button correctButton = rootView.findViewById(id);
      if (correctButton == null) {
        break missingId;
      }

      id = R.id.correct_counter;
      TextView correctCounter = rootView.findViewById(id);
      if (correctCounter == null) {
        break missingId;
      }

      id = R.id.exit_button;
      MaterialButton exitButton = rootView.findViewById(id);
      if (exitButton == null) {
        break missingId;
      }

      id = R.id.missed_button;
      Button missedButton = rootView.findViewById(id);
      if (missedButton == null) {
        break missingId;
      }

      id = R.id.missed_counter;
      TextView missedCounter = rootView.findViewById(id);
      if (missedCounter == null) {
        break missingId;
      }

      id = R.id.outOfText;
      TextView outOfText = rootView.findViewById(id);
      if (outOfText == null) {
        break missingId;
      }

      id = R.id.skip_button;
      Button skipButton = rootView.findViewById(id);
      if (skipButton == null) {
        break missingId;
      }

      return new ActivityStudySetBinding((ConstraintLayout) rootView, buttonLayout, card1, card2,
          card3, cardConstraintLayout, cardText, completedText, correctButton, correctCounter,
          exitButton, missedButton, missedCounter, outOfText, skipButton);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
