package cs.mad.flashcards.entities


data class Flashcard(
    var question: String,
    var answer: String,
    var missed: Boolean
) {
    companion object {
        fun getHardcodedFlashcards(): MutableList<Flashcard> {
            var cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Definition $i", missed = false))
            }
            return cards
        }
    }
}