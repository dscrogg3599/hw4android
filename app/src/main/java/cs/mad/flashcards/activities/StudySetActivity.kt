package cs.mad.flashcards.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.cardview.widget.CardView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class StudySetActivity : AppCompatActivity() {

    private val cards = Flashcard.getHardcodedFlashcards()
    var flipped = false
    var correctCount = 0
    var missedCount = 0
    var counting = 0
    var initialSize = cards.size

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_set)

        Log.d("debug", "Counting: " + counting.toString()
                + " Card Count: " + cards.size.toString())

        findViewById<Button>(R.id.exit_button).setOnClickListener {
            startActivity(Intent(this, FlashcardSetDetailActivity::class.java))
        }

        findViewById<TextView>(R.id.outOfText).text = counting.toString() + "/" + initialSize.toString()

        findViewById<TextView>(R.id.card_text).text = cards.get(0).question

        findViewById<CardView>(R.id.card3).setOnClickListener {
            if (!flipped) {
                findViewById<TextView>(R.id.card_text).text = cards.get(0).answer
                flipped = true
            }
            else {
                findViewById<TextView>(R.id.card_text).text = cards.get(0).question
                flipped = false
            }
        }

        findViewById<Button>(R.id.missed_button).setOnClickListener {

            //if unsolved cards remain
            if ((counting < initialSize) && (cards.size > 1)) {
                //moving the card at front to back
                cards.get(0).missed = true
                var missedCard = cards.get(0)
                cards.removeAt(0)
                cards.add(missedCard)
                //counting miss
                missedCount += 1
                findViewById<TextView>(R.id.missed_counter).text = "Missed: " + missedCount.toString()
                //refreshing data
                findViewById<TextView>(R.id.card_text).text = cards.get(0).question
                flipped = false
            }
            //otherwise, flash Set Completed
        }

        findViewById<Button>(R.id.skip_button).setOnClickListener {
            //if unsolved cards remained
            if ((counting < initialSize) && (cards.size > 1)) {
                //moving the card at front to back
                var skippedCard = cards.get(0)
                cards.removeAt(0)
                cards.add(skippedCard)
                //refreshing data
                findViewById<TextView>(R.id.card_text).text = cards.get(0).question
                flipped = false
            }
            //otherwise flash Set Completed
        }

        findViewById<Button>(R.id.correct_button).setOnClickListener {
            //if unsolved cards remain and the card has never been missed
            if ((counting < initialSize) && (cards.size > 1) && (cards.get(0).missed == false)) {
                //removing correct card
                cards.removeAt(0)
                //counting hit
                correctCount += 1
                counting += 1
                findViewById<TextView>(R.id.correct_counter).text = "Correct: " + correctCount.toString()
                findViewById<TextView>(R.id.outOfText).text = counting.toString() + "/" + initialSize.toString()
                //refreshing data
                findViewById<TextView>(R.id.card_text).text = cards.get(0).question
                flipped = false
            }
            //if unsolved cards remain and the card has been missed
            else if  (cards.get(0).missed == true) {
                //moving the card at front to back
                cards.get(0).missed = false
                var unmissedCard = cards.get(0)
                cards.removeAt(0)
                cards.add(unmissedCard)
                //refreshing data
                findViewById<TextView>(R.id.card_text).text = cards.get(0).question
                flipped = false
            }
            //otherwise flash set completed
            else {
                val junkCard: Flashcard = Flashcard("Set Completed!", "Set Completed!", false)
                cards.add(junkCard)
                cards.removeAt(0)
                findViewById<TextView>(R.id.correct_counter).text = "Correct: " + initialSize.toString()
                findViewById<TextView>(R.id.outOfText).text = initialSize.toString() + "/" + initialSize.toString()
                findViewById<TextView>(R.id.card_text).text = cards.get(0).question
            }
        }

    }
}