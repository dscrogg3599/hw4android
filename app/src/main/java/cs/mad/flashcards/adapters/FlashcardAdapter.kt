package cs.mad.flashcards.adapters

import android.content.Context
import android.content.DialogInterface
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding.inflate
import cs.mad.flashcards.databinding.ActivityMainBinding.inflate
import cs.mad.flashcards.databinding.ItemFlashcardSetBinding.inflate
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>, activity: FlashcardSetDetailActivity) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = dataSet.toMutableList()
    private val activity = activity



    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question
        val flashcardDialog = AlertDialog.Builder(viewHolder.itemView.context)
            .setTitle("Flashcard")
            .setMessage("Term: " + dataSet.get(position).question + "\nDefinition: " + dataSet.get(position).answer)
            .setNeutralButton("Edit") { dialogInterface: DialogInterface, i: Int ->
                showEditDialog(viewHolder, position)
            }
            .setPositiveButton("Done") { _, _ -> }
        viewHolder.itemView.setOnClickListener {
            flashcardDialog.create()
            flashcardDialog.show()
        }

        viewHolder.itemView.setOnLongClickListener {
            showEditDialog(viewHolder, position)
            true
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }

    fun showEditDialog(viewHolder: ViewHolder, position: Int) {
        val termView = activity.layoutInflater.inflate(R.layout.custom_term, null)
        val defView = activity.layoutInflater.inflate(R.layout.custom_definition, null)
        val termEditText = termView.findViewById<EditText>(R.id.custom_term)
        val defEditText = defView.findViewById<EditText>(R.id.custom_definition)
        termEditText.setText(dataSet.get(position).question)
        defEditText.setText(dataSet.get(position).answer)

        AlertDialog.Builder(viewHolder.itemView.context)
            .setCustomTitle(termView)
            .setView(defView)
            .setNeutralButton("Delete") { dialogInterface: DialogInterface, i: Int ->
                deleteCard(position)
            }
            .setPositiveButton("Done") { dialogInterface: DialogInterface, i: Int ->
                dataSet.get(position).question = termEditText.text.toString()
                dataSet.get(position).answer = defEditText.text.toString()
                notifyDataSetChanged()
            }

            .create()
            .show()

    }

    fun deleteCard(position: Int) {
        dataSet.removeAt(position)
        notifyDataSetChanged()
    }
}